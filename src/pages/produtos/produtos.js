angular.module('Cadastro', [])
.controller('UserController', UserController) ;

function UserController() {
var vm = this;
vm.titulo = "Produtos";
vm.users = [{nome:"Smartphone A12", preço:'1.299 R$'},
          {nome:"Smart TV 55", preço:'2.000 R$'},
          {nome:"Geladeira 375L", preço:'2.500 R$'},
          {nome:"Fogão", preço:'1.099 R$'},
          {nome:"Tablet", preço:'899.99 R$'},
          {nome:"Sofá", preço:'2.000 R$'},
          {nome:"Máquina de Lavar", preço:'1.500 R$'}
      
];

function getKeyLength() {
  try{
    return Object.keys(angular.copy(vm.users[0])).length;

  }catch(error){
   
  }

  
}
vm.keyLength = getKeyLength();

vm.add = add;
function add(user) {
  if(user.idade >= 5){
    vm.users.push(angular.copy(user));
  }
}

vm.edit= edit;
function edit(user) {
  vm.form = angular.copy(user);
}
}