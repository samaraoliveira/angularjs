
    angular.module('Cadastro', [])
      .controller('UserController', UserController) ;

    function UserController() {
      var vm = this;
      vm.titulo = "Cadastro de Clientes";
      vm.users = [];

      function getKeyLength() {
        try{
          return Object.keys(angular.copy(vm.users[0])).length;

        }catch(error){
          
        }    
      }
      vm.keyLength = getKeyLength();

      vm.add = add;
      function add(user) {
        if(user.idade >= 18){
          vm.users.push(angular.copy(user));
        }
      }

      vm.remove = remove;
      function remove(users) {
        vm.users = vm.users.filter(function(el){ return !el.selecionado});
      }

      vm.edit = function edit(user,index)  {
        vm.form.index = angular.copy(user);
        vm.form.index = index;
      }

      vm.save= save;
      function save(user) {
        var users = vm.users.map(function(el,i){
          
          if(i == user.index){
            delete user.index;
            return user;
          }
          return el;
        });
        vm.users = users
      }
    }
